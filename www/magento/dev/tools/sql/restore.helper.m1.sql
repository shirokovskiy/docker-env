-- After restore dump locally from production
-- START TRANSACTION;
SET FOREIGN_KEY_CHECKS = 0;

-- report_viewed_product_index
-- report_compared_product_index
-- report_event
-- catalog_compare_item

-- IF NO STAGING STATUS
-- catalog_category_flat_cl
-- catalog_category_product_cat_cl
-- catalog_category_product_index_cl
-- catalog_product_flat_cl
-- catalog_product_index_price_cl
-- cataloginventory_stock_status_cl
-- catalogsearch_fulltext_cl
-- enterprise_url_rewrite_category_cl
-- enterprise_url_rewrite_product_cl
-- enterprise_url_rewrite_redirect_cl

UPDATE `core_config_data` SET `value` = 'http://dm.site.dev/' WHERE `path` LIKE 'web/secure/base_url' OR `path` LIKE 'web/unsecure/base_url';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` LIKE 'dev/js/merge_files';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` LIKE 'dev/css/merge_css_files';
UPDATE `core_config_data` SET `value` = '1' WHERE `path` LIKE 'dev/log/active'; -- only M1
-- UPDATE `core_config_data` SET `value` = '' WHERE `path` LIKE 'design/head/includes'; -- remove counter scripts and other online services
-- UPDATE `core_config_data` SET `value` = '' WHERE `path` LIKE 'design/footer/absolute_footer'; -- remove GTM script
UPDATE `core_cache_option` SET `value` = 0; -- Turn OFF any cache types (only M1)

UPDATE `admin_user` SET `password` = CONCAT(md5("DDp@Ssw0rD12#"),":DD"), `modified` = NOW() WHERE `username` = 'admin' LIMIT 1;

TRUNCATE TABLE `log_customer`;
TRUNCATE TABLE `log_quote`;
TRUNCATE TABLE `log_summary`;
TRUNCATE TABLE `log_summary_type`;
TRUNCATE TABLE `log_url`;
TRUNCATE TABLE `log_url_info`;
TRUNCATE TABLE `log_visitor`;
TRUNCATE TABLE `log_visitor_info`;
TRUNCATE TABLE `log_visitor_online`;

SET FOREIGN_KEY_CHECKS = 1;
-- COMMIT;
