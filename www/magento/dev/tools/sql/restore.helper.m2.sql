-- After restore dump locally from production
-- START TRANSACTION;
SET FOREIGN_KEY_CHECKS = 0;
UPDATE `core_config_data` SET `value` = 'http://dm.site.dev/' WHERE `path` LIKE 'web/unsecure/base_url';
UPDATE `core_config_data` SET `value` = 'http://dm.site.dev/' WHERE `path` LIKE 'web/secure/base_url';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` LIKE 'web/secure/use_in_frontend';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` LIKE 'web/secure/use_in_adminhtml';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` LIKE 'dev/js/merge_files';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` LIKE 'dev/css/merge_css_files';
-- UPDATE `core_config_data` SET `value` = '' WHERE `path` LIKE 'design/head/includes'; -- remove counter scripts and other online services
-- UPDATE `core_config_data` SET `value` = '' WHERE `path` LIKE 'design/footer/absolute_footer'; -- remove GTM script

SET FOREIGN_KEY_CHECKS = 1;
-- COMMIT;
