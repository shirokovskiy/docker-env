#!/bin/bash
# Use it as ./dev/tools/validate-phpmd.sh -f ./vendor/brand-name/module-name
# If Docker container used, run it inside PHP container.
# In Magento Plugins used $subject & $result, mostly unused variables => use
# in doc-block @SuppressWarnings(PHPMD.UnusedFormalParameter)

SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
# go to current folder, to be sure we'll make two steps up from proper dir
cd "$THIS_DIR" || exit
cd "../.."
# we went to project root folder from ./dev/tools
WEB_DOCUMENT_ROOT="$(pwd)"

# Parse input params
while getopts "f:" opt; do
  case $opt in
    f) VALIDATION_FOLDER="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

[ -z "$WEB_DOCUMENT_ROOT" ] && WEB_DOCUMENT_ROOT="/var/www/html"

# Default folder/file to validate
[ -z "$VALIDATION_FOLDER" ] && VALIDATION_FOLDER="$WEB_DOCUMENT_ROOT/app/code"

echo "Validation folder: $VALIDATION_FOLDER"

if [[ -d "$VALIDATION_FOLDER" || -f "$VALIDATION_FOLDER" ]]; then
  php "$WEB_DOCUMENT_ROOT"/vendor/bin/phpmd \
      "$VALIDATION_FOLDER" text "$WEB_DOCUMENT_ROOT"/dev/tests/static/testsuite/Magento/Test/Php/_files/phpmd/ruleset.xml

  if [ $? != 0 ]; then
    msg="PHPMD validation failed!"
    printf "\033[31;43m$msg\033[0m\n" # colored output https://ansi.gabebanks.net/
    exit 1
  fi
fi

msg="PHPMD validation completed!"
printf "\033[32;43m$msg\033[0m\n" # colored output https://ansi.gabebanks.net/
