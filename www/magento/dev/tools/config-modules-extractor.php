<?php
/**
 * Get list of modules alphabetically sorted.
 * After each bin/magento setup:upgrade the file config.php has different sort of modules.
 * In case you change Git branches and run setup:upgrade again is hard to recognize which of modules are new
 * and which of modules resorted.
 *
 * This script helps you to get sorted list of modules from config.php and compare it with any other state of this file
 * in different branches, etc.
 *
 * @author: Dimitry Shirokovskiy <info@phpwebstudio.com>
 */
$arr = require_once "../../app/etc/config.php";

if (!isset($arr['modules'])) {
    echo 'Error: no modules found.' . PHP_EOL;
    exit(1);
}

$modules = $arr['modules'];

ksort($modules);

echo PHP_EOL;
foreach ($modules as $key => $value) {
    echo $key . PHP_EOL;
}
echo PHP_EOL;
