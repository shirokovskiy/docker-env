#!/usr/bin/env bash
date
echo "Start getting remote dump!"

REMOTE_DBNAME=production_database_name
REMOTE_SSH_NAME=production_ssh_host_alias
REMOTE_PRJ_DIR='/var/www/html/'

SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd "$THIS_DIR" || exit
cd "../.."
RF="$(pwd)"
FILENAME="$REMOTE_DBNAME".sql.7z
BACKUPS=$RF"/var/backups"

echo "Download database dump $FILENAME"
rsync -Lpgo --progress --stats $REMOTE_SSH_NAME:$REMOTE_PRJ_DIR/var/backups/$FILENAME "$BACKUPS"/

if [ -f "$BACKUPS"/$FILENAME ]
then
    rm -f "$BACKUPS"/$REMOTE_DBNAME.sql
    echo "Unzip database $FILENAME"
    cd "$BACKUPS" || exit
    7z x $FILENAME -so > $REMOTE_DBNAME.sql
else
    echo "No file "$FILENAME
fi
