#!/bin/bash
# Use it as ./dev/tools/validate-phpcs.sh -f ./vendor/brand-name/module-name
# or ./dev/tools/validate-phpcs.sh -f ./vendor/brand-name/module-name/path/to/file.php
# If Docker container used, run it inside PHP container.

SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
# go to current folder, to be sure we'll make two steps up from proper dir
cd "$THIS_DIR" || exit
cd "../.."
# we went to project root folder from ./dev/tools
WEB_DOCUMENT_ROOT="$(pwd)"

# Parse input params
while getopts "f:" opt; do
  case $opt in
    f) VALIDATION_FOLDER="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

[ -z "$WEB_DOCUMENT_ROOT" ] && WEB_DOCUMENT_ROOT="/var/www/html"

# Default folder/file to validate
[ -z "$VALIDATION_FOLDER" ] && VALIDATION_FOLDER="$WEB_DOCUMENT_ROOT/app/code"

echo "Run validation for: $VALIDATION_FOLDER"

if [[ -d "$VALIDATION_FOLDER" || -f "$VALIDATION_FOLDER" ]]; then
  php "$WEB_DOCUMENT_ROOT"/vendor/bin/phpcs -p \
      --standard="$WEB_DOCUMENT_ROOT"/dev/tests/static/framework/Magento/ruleset.xml "$VALIDATION_FOLDER"

  if [ $? != 0 ]; then
    msg="PHPCS validation failed!"
    printf "\033[31;43m$msg\033[0m\n" # colored output https://ansi.gabebanks.net/
    exit 1
  fi
fi

msg="PHPCS validation completed!"
printf "\033[32;43m$msg\033[0m\n" # colored output https://ansi.gabebanks.net/
