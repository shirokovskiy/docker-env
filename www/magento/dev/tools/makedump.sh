#!/bin/bash
date
echo "Start making dump!"
DBNAME=magedb
CUR_DATE=$(date +%Y-%m-%d---%H-%M)
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd "../.."
RD="$(pwd)"
cd $THIS_DIR
FILENAME=$DBNAME.$CUR_DATE.sql
BACKUPS=$RD/var/backups
LOCAL_DUMP=$DBNAME.sql.7z

EXCLUDED_TABLES=(
log_customer
log_quote
log_summary
log_summary_type
log_url
log_url_info
log_visitor
log_visitor_info
log_visitor_online
catalogsearch_fulltext_cl
catalog_category_product_index_cl
catalog_product_flat_cl
report_event
report_viewed_product_index
cataloginventory_stock_status_cl
catalog_product_index_price_cl
)

IGNORED_TABLES_STRING=''
for TABLE in "${EXCLUDED_TABLES[@]}"
do :
   IGNORED_TABLES_STRING+=" --ignore-table=${DBNAME}.${TABLE}"
done

# touch $RD/maintenance.flag # for M1 only

RMDAYS=7
echo "Remove dumps older $RMDAYS days"
find $BACKUPS -name '*.sql.7z' -type f -mtime +$RMDAYS -exec rm {} \;
#find $BACKUPS -name '*_db.gz' -type f -mtime +$RMDAYS -exec rm {} \;

if [ -f $BACKUPS/$FILENAME ];
then
    echo "Remove previous dump file "$FILENAME
    rm -f $BACKUPS/$FILENAME
fi

echo "Make structure dump of "$DBNAME" into "$FILENAME
# --skip-lock-tables
mysqldump --defaults-extra-file=$THIS_DIR/sql/dockerdb.cnf --single-transaction --no-data --routines $DBNAME > $BACKUPS/$FILENAME

echo "Make content dump of "$DBNAME" into "$FILENAME
mysqldump --defaults-extra-file=$THIS_DIR/sql/dockerdb.cnf --no-create-info --skip-triggers ${IGNORED_TABLES_STRING} $DBNAME >> $BACKUPS/$FILENAME

if [ -f $BACKUPS/$FILENAME ]
then
    echo "Archive dump"
    7z a -mx=9 $BACKUPS/$FILENAME.7z $BACKUPS/$FILENAME
fi
rm -rf $BACKUPS/$FILENAME

if [[ -h $BACKUPS/$LOCAL_DUMP ]] || [[ -f $BACKUPS/$LOCAL_DUMP ]]
then
    echo "Remove to repair symbolic link"
    rm -rf $BACKUPS/$LOCAL_DUMP
else
    echo "No such file "$BACKUPS/$LOCAL_DUMP
fi

cd $BACKUPS
if [ -f $FILENAME.7z ]
then
    echo "Make new link $LOCAL_DUMP -> $FILENAME.7z"
    ln -s $FILENAME.7z $LOCAL_DUMP
fi

rm -rf $RD/maintenance.flag &
echo "The End of making dump!"
date
