#!/usr/bin/env bash
DOCKERBOX="_PROJECT-NAME"

shopt -s expand_aliases
# shellcheck disable=SC1090
source ~/.bash_aliases

# in case on Linux
if [ -d ~/.cache/JetBrains ]; then
  echo "Check environment cache"
  find ~/.cache/JetBrains/ -name "content.dat.storageData" -exec ls -lsah {} \;
  echo "Clean it if too big!"
fi

SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd "$THIS_DIR" || exit

DOCKERS=`docker ps | grep -v grep | grep $DOCKERBOX | wc -l`

if [[ ${DOCKERS} -lt 1 ]]
then
    echo "WARNING - There is no Docker Containers $DOCKERBOX running for this project"

    TIME_LIMIT=15
    choice=""
    echo "Do you want to start docker$DOCKERBOX ? <y/n>"
    read -t $TIME_LIMIT choice

    if [ -n "$choice" ]
    then
	      case "$choice" in
          y|Y ) drun;; # drun - is alias from ~/.bash_aliases for '... && docker compose up -d' commands.
          n|N ) echo "No Docker, no run!"; exit 1;;
          * ) echo "invalid choice :( "; exit 1;;
        esac
    else
        echo "No choice to start Docker Containers"
        exit 1
    fi
fi

"$THIS_DIR"/getdump.sh
"$THIS_DIR"/restore.db4docker.sh
