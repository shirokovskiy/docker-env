#!/bin/bash
# Fix it for another project
REMOTE_DBNAME="production_database_name"
REMOTE_DBUSER="production_database_user"
DBNAME="magedb" # look at docker-compose.yml for database & username
DBUSER="dbuser"




# Parameters
# 1 - path to dump-file (optional)
# 2 - database name (optional)
date
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
echo "Current folder: ${THIS_DIR}"
cd "$THIS_DIR" || exit
cd "../.."
RF="$(pwd)"
BACKUPS="$RF/var/backups"

# Если число параметров равно 1 или 2
if [ $# -eq 1 ] || [ $# -eq 2 ]
then
    DBFILE=$1

    if [ $# -eq 2 ]
    then
        DBNAME=$2
    fi
else
    DBFILE=$BACKUPS/$REMOTE_DBNAME.sql
fi

echo "Current folder: $(pwd)"
echo "Dump file: $DBFILE"
echo "Database name: $DBNAME"

if [ ! -f "$DBFILE" ] && [ -f "$DBFILE".bz2 ]
then
    echo "Archive exists > unzip dump"
    cd "$BACKUPS" || exit
    bunzip2 "$DBFILE".bz2
    cd "$THIS_DIR" || exit
fi

if [ ! -f "$DBFILE" ] && [ -f "$DBFILE".7z ]
then
    echo "Unzip database $FILENAME"
    cd "$BACKUPS" || exit
    if hash 7z 2>/dev/null; then
        7z x "$DBFILE".7z -so > $REMOTE_DBNAME.sql
    else
        echo "Error: 7-Zip not installed!"
        exit 1;
    fi

    cd "$THIS_DIR" || exit
fi

#echo "Drop database"
#mysql --defaults-extra-file=$THIS_DIR/sql/dockerdb.cnf -e "DROP DATABASE $DBNAME;"

#echo "Create database"
#mysql --defaults-extra-file=$THIS_DIR/sql/dockerdb.cnf -e "CREATE DATABASE $DBNAME CHARACTER SET utf8 COLLATE utf8_general_ci;"

function fixDefiner {
  echo "Fix user access in ${DBFILE}"
  grep 'DEFINER=' "${DBFILE}"
  echo 'DEFINER BEFORE'
  SIZE_BEFORE=$(du -s "${DBFILE}")

  SED="s/\`${REMOTE_DBUSER}\`@\`localhost\`/\`${DBUSER}\`@\`%\`/g"
  echo "Expression to execute: "${SED}
  LC_ALL=C sed -i.bkp ${SED} "${DBFILE}"

  SED="s/\`${REMOTE_DBUSER}\`@\`%\`/\`${DBUSER}\`@\`%\`/g"
  echo "Expression to execute: "${SED}
  LC_ALL=C sed -i.bkp ${SED} "${DBFILE}"

  grep 'DEFINER=' "${DBFILE}"
  echo 'DEFINER AFTER'

  echo "File size before and after changes $SIZE_BEFORE vs. $(du -s "${DBFILE}")"
}

fixDefiner

echo "Restore database from file ${DBFILE} to local MySQL at <${DBNAME}> database"
pv "${DBFILE}" | mysql --defaults-extra-file="$THIS_DIR/sql/dockerdb.cnf" --ssl-mode=disabled -f "${DBNAME}"
echo "Execute helper"
pv "${THIS_DIR}"/sql/restore.helper.sql | mysql --defaults-extra-file=$THIS_DIR"/sql/dockerdb.cnf" --ssl-mode=disabled -f "${DBNAME}"
date
