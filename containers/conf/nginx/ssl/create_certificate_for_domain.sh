#!/usr/bin/env bash

SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
echo "Current folder: ${THIS_DIR}"
cd "$THIS_DIR" || exit

if [ -z "$1" ]
then
  echo "Please supply a subdomain to create a certificate for";
  echo "e.g.: $SOURCE dm.website.com"
  exit;
fi

if [ -f device.key ]; then
  KEY_OPT="-key"
else
  KEY_OPT="-keyout"
fi

DOMAIN=$1
COMMON_NAME=${2:-$1}

SUBJECT="/C=CA/ST=None/L=NB/O=None/CN=$COMMON_NAME"
NUM_OF_DAYS=999

if [ ! -x "$(command -v openssl)" ]
then
    echo "Error: OpenSSL not installed! Run: sudo apt-get install openssl"
    exit 1
fi
openssl req -new -newkey rsa:2048 -sha256 -nodes $KEY_OPT device.key -subj "$SUBJECT" -out device.csr

cat service-files/v3.ext | sed s/%%DOMAIN%%/$COMMON_NAME/g > /tmp/__v3.ext

openssl x509 -req -in device.csr -CA service-files/rootCA.pem -CAkey service-files/rootCA.key -CAcreateserial -out device.crt -days $NUM_OF_DAYS -sha256 -extfile /tmp/__v3.ext

cp device.crt $DOMAIN.crt

# remove temp file
rm -f device.crt device.csr

echo "Certificate successfully created! To add to system certificates double-click *.crt file"
