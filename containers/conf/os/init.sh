#!/bin/bash
echo "---=== CONTAINER CUSTOMISATION ===---"

apt update && apt upgrade -y && apt autoremove && apt install sudo -y

if [ ! -d /home/app ]; then
  useradd -m -G www-data app
  usermod -a -G app www-data
fi

if [ -d /home/app ]; then
  if [ ! -f /home/app/.bash_aliases ]; then
    echo "alias ll='ls -halF --group-directories-first'" >> /home/app/.bash_aliases
  fi

  if [ ! -f /home/app/.ssh/id_rsa ]; then
    sudo -u app ssh-keygen -q -t rsa -N '' -f /home/app/.ssh/id_rsa <<< y
  fi
fi

if [ ! -f /root/.bash_aliases ]; then
  echo "alias ll='ls -halF --group-directories-first'" >> /root/.bash_aliases
fi

if [ -f /root/.bashrc ]; then
  sed -i -e "/# export LS_OPTIONS/s/^# //" /root/.bashrc
  sed -i -e "/# alias ls=/s/^# //" /root/.bashrc

  if ! grep -q bash_aliases /root/.bashrc; then
    echo ". ~/.bash_aliases" >> /root/.bashrc
  fi
fi

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

nvm install 10.23.0
npm install -g gulp
npm install -g grunt

docker-php-entrypoint php-fpm
