# Useful notes

## Branches

master - for Linux  
macbookProM1 - for MacOS  

## Prepare docker-compose.yml

Edit file docker-compose.yml: replace PROJECT-NAME to your needed project name.

## Prepare Composer

Edit file ./containers/conf/composer/auth.json: fill needed usernames, passwords, tokens, keys.

## Check PHP modules installed for Magento 2

Run this commands to check if necessary modules is installed:  
$ docker-compose up -d  
$ docker-compose exec php_box bash    
$ export MAGE_PHP_REQ_MODULES='bcmath|curl|ctype|dom|gd|hash|iconv|intl|mbstring|openssl|pdo_mysql|simplexml|spl|zip|libxml|xmlwriter|pcre|sockets|json|soap|sodium|xsl|opcache' && modsArray=(\`echo $MAGE_PHP_REQ_MODULES | tr '|' ' '\`) && modsCount=(${#modsArray[@]}) && \
php -m | grep -iE "$MAGE_PHP_REQ_MODULES" | sort --unique | wc -l | xargs -I% test % -eq $modsCount && echo "ALL GOOD" || echo "ATTENTION! Not enough PHP modules for Magento! Check it by command: php -m"  

> _As a result you'll see "ALL GOOD" or "ATTENTION ... " message._

## Prepare web-container for Magento development

Run as root in Docker container (php_box):

$ curl -fsSL https://deb.nodesource.com/setup_16.x | bash -

$ apt update && apt install -y nodejs

$ apt update && apt upgrade -y && apt autoremove && apt install -y g++ git nano iputils-ping \  
wget unzip ssh libxml2-dev libxslt-dev zlib1g-dev libzip-dev libpng-dev \  
libjpeg62-turbo-dev libfreetype6-dev libicu-dev libmcrypt-dev libxpm-dev \  
libwebp-dev libonig-dev libpq-dev libcurl4-openssl-dev texlive-fonts-recommended  
$ apt install -y imagemagick libmagickwand-dev --no-install-recommends  
$ test -e /usr/local/etc/php/php.ini-development && \  
cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini && \  
pear config-set php_ini /usr/local/etc/php/php.ini || \  
echo "NO-PHP-INI"; \  
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer; \  
docker-php-ext-install -j$(nproc) iconv && \  
docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp && \  
docker-php-ext-install -j$(nproc) gd && \  
docker-php-ext-install zip pdo pdo_mysql intl soap bcmath xsl sockets mbstring opcache curl && \  
printf "\n" | pecl install imagick && docker-php-ext-enable imagick; \  
printf "\n" | pecl install mcrypt; \  
npm install -g grunt-cli;  
  
Next command only if you need XDebug    
$ pecl install xdebug-2.9.8 || docker-php-ext-install xdebug

## How to create Volume for Magento project (if necessary)

$ docker volume create magento_web_volume  
$ docker run --privileged --rm -v magento_web_volume:/web alpine /bin/sh -c "chown 1000:1000 -R /var/www/html && chmod -R g+s /var/www/html && chmod -R 777 /var/www/html"
